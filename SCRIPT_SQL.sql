/*
-------------------------------------------------------------------------------------------------------------------------------------------------------------
MOTOR DE BASE DE DATOS: ORACLE
SE SOLICITA MANTENER LA CLAVE 123456789 YA QUE EN EL CODIGO LA CONEXION DEL BACK TIENE ESTA CLAVE, EN CASO DE CAMBIO AJUSTAR EL PROPETIES CON LA NUEVA CLAVE.
-------------------------------------------------------------------------------------------------------------------------------------------------------------
*/

/*
CREACION DE USUARIO CON PERMISOS DE DBA CON OBJETO DE PRUEBA
*/
CREATE USER PRUEBA IDENTIFIED BY 123456789;
GRANT DBA TO PRUEBA;


/*
CREACION DE TABLA DE TAREA
*/
CREATE TABLE PRUEBA.TAREA
(
TARE_ID NUMBER GENERATED ALWAYS AS IDENTITY,
TARE_DESCRIPCION VARCHAR2(4000) NOT NULL,
TARE_FECHACREACION DATE DEFAULT SYSDATE NOT NULL,
TARE_VIGENTE VARCHAR2(1) NOT NULL,
CONSTRAINT TARE_PK PRIMARY KEY (TARE_ID),
CONSTRAINT TARE_VIGENTE_CK CHECK (TARE_VIGENTE IN ('0','1'))
);


/*
COMENTARIOS SOBRE COLUMNAS Y TABLA
*/
COMMENT ON COLUMN PRUEBA.TAREA.TARE_ID
IS
'identificador de la tabla (autonumerico)';

COMMENT ON COLUMN PRUEBA.TAREA.TARE_DESCRIPCION
IS
'descripcion de la tarea';

COMMENT ON COLUMN PRUEBA.TAREA.TARE_FECHACREACION
IS
'fecha de insercion del registro en la base de datos';

COMMENT ON COLUMN PRUEBA.TAREA.TARE_VIGENTE
IS
'la tarea se encuentra vigente (0= no vigente, 1= vigente)';

COMMENT ON TABLE PRUEBA.TAREA
IS
'contiene los registros de las tareas';

