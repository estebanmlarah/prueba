package co.soaint.prueba.entities.tarea;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "TAREA", schema="PRUEBA")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TareaDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "TARE_ID")
    private BigInteger id;

    @Column(name = "TARE_DESCRIPCION", nullable=false, length=4000)
    private String descripcion;

    @Column(name = "TARE_VIGENTE", nullable=false, length=1)
    private String vigente;

    @Temporal(TemporalType.DATE)
    @Column(name = "TARE_FECHACREACION", insertable = false, updatable = false, length=7)
    private Date fechaCreacion;

}
