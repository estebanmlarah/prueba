package co.soaint.prueba.dto.tarea;

import co.soaint.prueba.utils.MensajeValidacion;
import co.soaint.prueba.utils.Regex;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.math.BigInteger;

@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TareaDTOActualizarRequest {

    @NotNull(message = MensajeValidacion.MSN_VARIABLE_REQUERIDA)
    private BigInteger id;

    @NotNull(message = MensajeValidacion.MSN_VARIABLE_REQUERIDA)
    @Size(min = 1, max = 4000, message = MensajeValidacion.MSN_ERROR_LONGITUD_1_4000)
    private String descripcion;

    @NotNull(message = MensajeValidacion.MSN_VARIABLE_REQUERIDA)
    @Pattern(regexp = Regex.REGEX_VIGENTE, message = MensajeValidacion.MSN_ERROR_ESTADO_VIGENTE)
    @Size(min = 1, max = 1, message = MensajeValidacion.MSN_ERROR_LONGITUD_1_1)
    private String vigente;

}
