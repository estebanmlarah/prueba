package co.soaint.prueba.dto.tarea;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.math.BigInteger;
import java.util.Date;

@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TareaDTO {

    private BigInteger id;

    private String descripcion;

    private String vigente;

    private Date fechaCreacion;

}
