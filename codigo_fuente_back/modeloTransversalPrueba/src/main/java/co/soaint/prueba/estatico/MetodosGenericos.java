package co.soaint.prueba.estatico;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


public class MetodosGenericos {

    private MetodosGenericos() {
        throw new IllegalStateException("Utility class");
    }

    public static String getIpPublicaCliente(HttpServletRequest request) {
        if (request.getHeader("X-Forwarded-For") != null) {
            return request.getHeader("X-Forwarded-For");
        } else {
            return request.getRemoteAddr();
        }
    }

    public static Map<String, String> getInformacionNavegador(HttpServletRequest request) {
        HashMap<String, String> variablesIngreso = new HashMap<>();
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        OperatingSystem agent = userAgent.getOperatingSystem();
        Browser browser = userAgent.getBrowser();
        Version version = userAgent.getBrowserVersion();

        if (agent != null) {
            variablesIngreso.put("SO", agent.getName());
            variablesIngreso.put("DISPOSITIVO", agent.getDeviceType().getName());
        }
        if (browser != null) {
            variablesIngreso.put("NAVEGADOR", browser.getName());
        }
        if (version != null) {
            variablesIngreso.put("VERSION", version.getVersion());
        }
        return variablesIngreso;
    }

    public static Map<String, String> generarMensajeErrorRequest(MethodArgumentNotValidException ex) {
        Map<String, String> errores = new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(error ->
                errores.put(error.getField(), error.getDefaultMessage()));

        return errores;
    }

}
