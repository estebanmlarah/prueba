package co.soaint.prueba.utils;

import co.soaint.prueba.estatico.MetodosGenericos;
import org.slf4j.Logger;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public final class MensajeGenerico {


    private MensajeGenerico() {
        throw new IllegalStateException("Utility class");
    }

    public static final void generarMensajeEntradaLog(Logger logger,HttpServletRequest request, String nombreMetodo, String objetoEntrada){
        String ip = MetodosGenericos.getIpPublicaCliente(request);
        Map<String, String> variablesIngreso = MetodosGenericos.getInformacionNavegador(request);
        String mensajeIngreso = "\n[\n\tTipo        [Entrada]\n\tIp usuario  ["+ip+"]\n\tSO          ["+variablesIngreso.get("SO")+"]\n\tDispositivo ["+variablesIngreso.get("DISPOSITIVO")+"]\n\tNavegador   ["+variablesIngreso.get("NAVEGADOR")+"]\n\tVersion Nav ["+ (variablesIngreso.get("VERSION") !=null ? variablesIngreso.get("VERSION") :"No encontrado") +"]\n][\n\tMetodo ["+nombreMetodo+"]\n\tObjeto ["+objetoEntrada+"]\n]\n";
        logger.info(mensajeIngreso);
    }

    public static final void generarMensajeSalidaLog(Logger logger,HttpServletRequest request, String nombreMetodo, String objetoEntrada){
        String ip = MetodosGenericos.getIpPublicaCliente(request);
        Map<String, String> variablesIngreso = MetodosGenericos.getInformacionNavegador(request);
        String mensajeSalida = "\n[\n\tTipo        [Salida]\n\tIp usuario  ["+ip+"]\n\tSO          ["+variablesIngreso.get("SO")+"]\n\tDispositivo ["+variablesIngreso.get("DISPOSITIVO")+"]\n\tNavegador   ["+variablesIngreso.get("NAVEGADOR")+"]\n\tVersion Nav ["+ (variablesIngreso.get("VERSION") !=null ? variablesIngreso.get("VERSION") :"No encontrado") +"]\n][\n\tMetodo ["+nombreMetodo+"]\n\tObjeto ["+objetoEntrada+"]\n]\n";
        logger.info(mensajeSalida);
    }

    public static final Map<String, String> generarMensajeErrorRequest(Logger logger, MethodArgumentNotValidException ex){
        Map<String, String> errores = MetodosGenericos.generarMensajeErrorRequest(ex);
        logger.error("Error de ingreso por Request \n[\n\t Mensaje: {}\n\tErrores: {}\n]",ex.getMessage(),errores);
        return errores;
    }



}
