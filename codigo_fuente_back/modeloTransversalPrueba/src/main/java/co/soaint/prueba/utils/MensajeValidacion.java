package co.soaint.prueba.utils;

public final class MensajeValidacion {

    private MensajeValidacion() {
        throw new IllegalStateException("Utility class");
    }

    public static final String MSN_VARIABLE_REQUERIDA = "La variable es requerida";
    public static final String MSN_ERROR_LONGITUD_1_1 = "La longitud debe ser de 1 caracter";
    public static final String MSN_ERROR_LONGITUD_1_4000 = "La longitud debe ser entre 1 y 4000 caracteres";
    public static final String MSN_ERROR_ESTADO_VIGENTE = "Los valores permitidos para el campo vigente es 1=vigente, 0=no vigente";

}
