package co.soaint.prueba.utils;

public final class Regex {

    private Regex() {
        throw new IllegalStateException("Utility class");
    }

    public static final String REGEX_VIGENTE = "^[01]*$";

}
