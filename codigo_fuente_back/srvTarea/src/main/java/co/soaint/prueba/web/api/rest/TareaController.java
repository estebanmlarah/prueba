package co.soaint.prueba.web.api.rest;

import co.soaint.prueba.dto.generic.GenericResponseDTO;
import co.soaint.prueba.dto.tarea.TareaDTOActualizarRequest;
import co.soaint.prueba.dto.tarea.TareaDTONuevoRequest;
import co.soaint.prueba.service.ITareaService;
import co.soaint.prueba.utils.MensajeGenerico;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigInteger;
import java.util.Map;

@RestController
@RequestMapping()
@CrossOrigin
public class TareaController {

    private static final Logger logger = LoggerFactory.getLogger(TareaController.class);

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private ITareaService iTareaService;

    @GetMapping(path = "/tarea",produces = "application/json")
    @ApiOperation(value = "Listar todas las tareas registradas en el sistema", notes = "notas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La consulta de las tareas fue exitosa", response = GenericResponseDTO.class)
    })
    public ResponseEntity<GenericResponseDTO> listar(HttpServletRequest request){
        try {

            MensajeGenerico.generarMensajeEntradaLog(logger, request, "listar", null);

            GenericResponseDTO genericResponseDTO = iTareaService.listar();

            MensajeGenerico.generarMensajeSalidaLog(logger, request, "listar", mapper.writeValueAsString(genericResponseDTO));

            return new ResponseEntity<>(
                    genericResponseDTO, HttpStatus.valueOf(genericResponseDTO.getStatusCode())
            );

        } catch (ResponseStatusException | HttpClientErrorException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "No se encuentra informacion");
        } catch (Exception e) {
            logger.error(Thread.currentThread().getStackTrace()[1].getMethodName(), e);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Ocurrio un error inesperado");
        }
    }

    @PostMapping(path = "/tarea",consumes = "application/json",produces = "application/json")
    @ApiOperation(value = "Crea una nueva tarea en el sistema", notes = "notas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La creacion de la tarea fue exitosa", response = GenericResponseDTO.class)
    })
    public ResponseEntity<GenericResponseDTO> crear(@Valid @RequestBody TareaDTONuevoRequest tareaDTONuevoRequest, HttpServletRequest request){
        try {

            MensajeGenerico.generarMensajeEntradaLog(logger, request, "crear", mapper.writeValueAsString(tareaDTONuevoRequest));

            GenericResponseDTO genericResponseDTO = iTareaService.crear(tareaDTONuevoRequest);

            MensajeGenerico.generarMensajeSalidaLog(logger, request, "crear", mapper.writeValueAsString(genericResponseDTO));

            return new ResponseEntity<>(
                    genericResponseDTO, HttpStatus.valueOf(genericResponseDTO.getStatusCode())
            );

        } catch (ResponseStatusException | HttpClientErrorException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "No se encuentra informacion");
        } catch (Exception e) {
            logger.error(Thread.currentThread().getStackTrace()[1].getMethodName(), e);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Ocurrio un error inesperado");
        }
    }

    @PutMapping(path = "/tarea",consumes = "application/json",produces = "application/json")
    @ApiOperation(value = "Actualiza una nueva tarea en el sistema", notes = "notas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La actualizacion de la tarea fue exitosa", response = GenericResponseDTO.class)
    })
    public ResponseEntity<GenericResponseDTO> actualizar(@Valid @RequestBody TareaDTOActualizarRequest tareaDTONuevoRequest, HttpServletRequest request){
        try {

            MensajeGenerico.generarMensajeEntradaLog(logger, request, "actualizar", mapper.writeValueAsString(tareaDTONuevoRequest));

            GenericResponseDTO genericResponseDTO = iTareaService.actualizar(tareaDTONuevoRequest);

            MensajeGenerico.generarMensajeSalidaLog(logger, request, "actualizar", mapper.writeValueAsString(genericResponseDTO));

            return new ResponseEntity<>(
                    genericResponseDTO, HttpStatus.valueOf(genericResponseDTO.getStatusCode())
            );

        } catch (ResponseStatusException | HttpClientErrorException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "No se encuentra informacion");
        } catch (Exception e) {
            logger.error(Thread.currentThread().getStackTrace()[1].getMethodName(), e);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Ocurrio un error inesperado");
        }
    }

    @DeleteMapping(path = "/tarea/{id}",produces = "application/json")
    @ApiOperation(value = "Eliminar una nueva tarea por id en el sistema", notes = "notas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La eliminacion de la tarea fue exitosa", response = GenericResponseDTO.class)
    })
    public ResponseEntity<GenericResponseDTO> eliminar(@PathVariable(value = "id", required = true)  BigInteger id, HttpServletRequest request){
        try {

            MensajeGenerico.generarMensajeEntradaLog(logger, request, "eliminar", id.toString());

            GenericResponseDTO genericResponseDTO = iTareaService.eliminar(id);

            MensajeGenerico.generarMensajeSalidaLog(logger, request, "eliminar", mapper.writeValueAsString(genericResponseDTO));

            return new ResponseEntity<>(
                    genericResponseDTO, HttpStatus.valueOf(genericResponseDTO.getStatusCode())
            );

        } catch (ResponseStatusException | HttpClientErrorException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "No se encuentra informacion");
        } catch (Exception e) {
            logger.error(Thread.currentThread().getStackTrace()[1].getMethodName(), e);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Ocurrio un error inesperado");
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<GenericResponseDTO> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        Map<String, String> errors = MensajeGenerico.generarMensajeErrorRequest(logger, ex);
        return new ResponseEntity<>(
                GenericResponseDTO.builder().message("Error en los datos ingresados").objectResponse(errors).statusCode(HttpStatus.NOT_FOUND.value()).build(), HttpStatus.NOT_FOUND
        );
    }
}
