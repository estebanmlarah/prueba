package co.soaint.prueba.commons.converter;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("co.soaint.prueba")
@PropertySource("classpath:application.properties")
public class ConfiguradorSpring {

}
