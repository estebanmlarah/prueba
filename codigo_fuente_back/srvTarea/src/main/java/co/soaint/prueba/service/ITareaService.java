package co.soaint.prueba.service;

import co.soaint.prueba.dto.generic.GenericResponseDTO;
import co.soaint.prueba.dto.tarea.TareaDTOActualizarRequest;
import co.soaint.prueba.dto.tarea.TareaDTONuevoRequest;
import java.math.BigInteger;

public interface ITareaService {

    GenericResponseDTO listar();

    GenericResponseDTO crear(TareaDTONuevoRequest tareaDTONuevoRequest);

    GenericResponseDTO actualizar(TareaDTOActualizarRequest tareaDTOActualizarRequest);

    GenericResponseDTO eliminar(BigInteger idTarea);

}
