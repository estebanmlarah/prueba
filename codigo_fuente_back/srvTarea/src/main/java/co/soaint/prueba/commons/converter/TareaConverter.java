package co.soaint.prueba.commons.converter;

import co.soaint.prueba.dto.tarea.TareaDTO;
import co.soaint.prueba.dto.tarea.TareaDTOActualizarRequest;
import co.soaint.prueba.dto.tarea.TareaDTONuevoRequest;
import co.soaint.prueba.entities.tarea.TareaDAO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TareaConverter {

    @Autowired
    private ModelMapper modelMapper;

    public TareaDAO tareaDTONuevoRequestToTareaDAO(TareaDTONuevoRequest tareaDTONuevoRequest){
        TareaDAO tareaDAO = new TareaDAO();
        modelMapper.map(tareaDTONuevoRequest, tareaDAO);
        return tareaDAO;
    }

    public TareaDAO tareaDTOActualizarRequestToTareaDAO(TareaDTOActualizarRequest tareaDTOActualizarRequest){
        TareaDAO tareaDAO = new TareaDAO();
        modelMapper.map(tareaDTOActualizarRequest, tareaDAO);
        return tareaDAO;
    }

    public TareaDTO tareaDAOtoDTO(TareaDAO tareaDAO){
        TareaDTO tareaDTO = new TareaDTO();
        modelMapper.map(tareaDAO, tareaDTO);
        return tareaDTO;
    }

    public List<TareaDTO> tareaDAOtoDTO(List<TareaDAO> listaTareaDAO) {
        List<TareaDTO> listaTareaDTO = new ArrayList<>();

        listaTareaDAO.stream().forEach(tareaDAO ->
                listaTareaDTO.add(this.tareaDAOtoDTO(tareaDAO))
        );

        return listaTareaDTO;
    }

}
