package co.soaint.prueba.repository;

import co.soaint.prueba.entities.tarea.TareaDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.math.BigInteger;

@Repository
public interface ITareaRepository extends JpaRepository<TareaDAO, BigInteger> {

}
