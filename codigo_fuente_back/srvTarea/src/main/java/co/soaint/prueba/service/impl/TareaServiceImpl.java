package co.soaint.prueba.service.impl;


import co.soaint.prueba.dto.generic.GenericResponseDTO;
import co.soaint.prueba.dto.tarea.TareaDTO;
import co.soaint.prueba.dto.tarea.TareaDTOActualizarRequest;
import co.soaint.prueba.dto.tarea.TareaDTONuevoRequest;
import co.soaint.prueba.entities.tarea.TareaDAO;
import co.soaint.prueba.service.ITareaService;
import co.soaint.prueba.commons.converter.TareaConverter;
import co.soaint.prueba.repository.ITareaRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;


@Service
public class TareaServiceImpl implements ITareaService {

    private final ITareaRepository iTareaRepository;

    private final TareaConverter tareaConverter;


    private static final Logger logger = LoggerFactory.getLogger(TareaServiceImpl.class);

    private ObjectMapper mapper = new ObjectMapper();

    public TareaServiceImpl(TareaConverter tareaConverter, ITareaRepository iTareaRepository) {
        this.tareaConverter = tareaConverter;
        this.iTareaRepository = iTareaRepository;
    }

    @Override
    public GenericResponseDTO listar(){
        try {

            List<TareaDAO> listaTareaDAO = iTareaRepository.findAll();

            if(listaTareaDAO.isEmpty()){
                return GenericResponseDTO.builder().message("No existen tareas creadas en el sistema").objectResponse(null).statusCode(HttpStatus.OK.value()).build();
            }

            List<TareaDTO> listaTareaDTO = tareaConverter.tareaDAOtoDTO(listaTareaDAO);


            String response = mapper.writeValueAsString(listaTareaDTO);

            logger.info("Lista de tareas realizada con exito. Resultado {}", response);

            return GenericResponseDTO.builder().message("Las tareas fueron consultadas correctamente").objectResponse(listaTareaDTO).statusCode(HttpStatus.OK.value()).build();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return GenericResponseDTO.builder().message("Error al intentar consultar las tareas").objectResponse(null).statusCode(HttpStatus.BAD_REQUEST.value()).build();
        }
    }

    @Override
    public GenericResponseDTO crear(TareaDTONuevoRequest tareaDTONuevoRequest){
        try {
            String request = mapper.writeValueAsString(tareaDTONuevoRequest);

            logger.info("Registrara una nueva tarea. Request {}", request);

            TareaDAO tareaDAO = tareaConverter.tareaDTONuevoRequestToTareaDAO(tareaDTONuevoRequest);

            tareaDAO = iTareaRepository.save(tareaDAO);

            request = mapper.writeValueAsString(tareaDAO);

            logger.info("Tarea creada con exito. Resultado {}", request);

            TareaDTO tareaDTO = tareaConverter.tareaDAOtoDTO(tareaDAO);

            return GenericResponseDTO.builder().message("La tarea fue creada correctamente").objectResponse(tareaDTO).statusCode(HttpStatus.OK.value()).build();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return GenericResponseDTO.builder().message("Error al intentar crear la tarea").objectResponse(null).statusCode(HttpStatus.BAD_REQUEST.value()).build();
        }
    }

    @Override
    public GenericResponseDTO actualizar(TareaDTOActualizarRequest tareaDTOActualizarRequest){
        try {

            String request = mapper.writeValueAsString(tareaDTOActualizarRequest);

            logger.info("Actualizara la tarea. Request {}", request);

            Optional<TareaDAO> tareaDAOTmp = iTareaRepository.findById(tareaDTOActualizarRequest.getId());

            if(!tareaDAOTmp.isPresent()){
                return GenericResponseDTO.builder().message("Error. No se encuentra la tarea con el id ingresado").objectResponse(null).statusCode(HttpStatus.BAD_REQUEST.value()).build();
            }

            TareaDAO tareaDAO = tareaConverter.tareaDTOActualizarRequestToTareaDAO(tareaDTOActualizarRequest);

            iTareaRepository.save(tareaDAO);

            request = mapper.writeValueAsString(tareaDAO);

            logger.info("Tarea actualizada con exito. Resultado {}", request);

            TareaDTO tareaDTO = tareaConverter.tareaDAOtoDTO(tareaDAO);

            return GenericResponseDTO.builder().message("La tarea fue actualizada correctamente").objectResponse(tareaDTO).statusCode(HttpStatus.OK.value()).build();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return GenericResponseDTO.builder().message("Error al intentar actualizar la tarea").objectResponse(null).statusCode(HttpStatus.BAD_REQUEST.value()).build();
        }
    }

    @Override
    public GenericResponseDTO eliminar(BigInteger idTarea){
        try {

            logger.info("Eliminara la tarea con id {}", idTarea);

            Optional<TareaDAO> tareaDAOTmp = iTareaRepository.findById(idTarea);

            if(!tareaDAOTmp.isPresent()){
                return GenericResponseDTO.builder().message("Error. No se encuentra la tarea con el id ingresado").objectResponse(null).statusCode(HttpStatus.BAD_REQUEST.value()).build();
            }

            iTareaRepository.delete(tareaDAOTmp.get());

            logger.info("Proceso de eliminacion de la tarea con id {} realizada correctamente.", idTarea);

            return GenericResponseDTO.builder().message("La tarea fue eliminada correctamente").objectResponse(idTarea).statusCode(HttpStatus.OK.value()).build();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return GenericResponseDTO.builder().message("Error al intentar eliminar la tarea").objectResponse(null).statusCode(HttpStatus.BAD_REQUEST.value()).build();
        }
    }

}
