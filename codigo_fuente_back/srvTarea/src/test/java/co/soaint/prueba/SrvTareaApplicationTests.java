package co.soaint.prueba;

import co.soaint.prueba.commons.converter.TareaConverter;
import co.soaint.prueba.dto.generic.GenericResponseDTO;
import co.soaint.prueba.dto.tarea.TareaDTO;
import co.soaint.prueba.dto.tarea.TareaDTOActualizarRequest;
import co.soaint.prueba.dto.tarea.TareaDTONuevoRequest;
import co.soaint.prueba.entities.tarea.TareaDAO;
import co.soaint.prueba.repository.ITareaRepository;
import co.soaint.prueba.service.impl.TareaServiceImpl;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.hamcrest.Matchers.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import static org.hamcrest.MatcherAssert.assertThat;


@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
class SrvTareaApplicationTests {

	@Autowired
	private TareaServiceImpl iTareaService;

	@Mock
	private TareaConverter tareaConverter;

	//@Mock
	//private ModelMapper modelMapper;

	@Mock
	private ITareaRepository tareaRepository;

	@Rule
	public MockitoRule rule = MockitoJUnit.rule();

	@Test
	void crear() throws Exception{
		TareaDTONuevoRequest tareaDTONuevoRequest = TareaDTONuevoRequest.builder().descripcion("TAREA TEST").vigente("1").build();
		TareaDAO tareaDAOConverter = TareaDAO.builder().descripcion("TAREA TEST").vigente("1").build();
		TareaDAO tareaDAO = TareaDAO.builder().id(BigInteger.ONE).descripcion("TAREA TEST").vigente("1").fechaCreacion(new Date()).build();
		TareaDTO tareaDTOConverter = TareaDTO.builder().id(BigInteger.ONE).descripcion("TAREA TEST").vigente("1").fechaCreacion(new Date()).build();

		when(tareaConverter.tareaDTONuevoRequestToTareaDAO(any(TareaDTONuevoRequest.class))).thenReturn(tareaDAOConverter);
		when(tareaRepository.save(any(TareaDAO.class))).thenReturn(tareaDAO);
		when(tareaConverter.tareaDAOtoDTO(any(TareaDAO.class))).thenReturn(tareaDTOConverter);

		iTareaService = new TareaServiceImpl(tareaConverter , tareaRepository);

		GenericResponseDTO genericResponseDTO = iTareaService.crear(tareaDTONuevoRequest);

		assertNotNull(genericResponseDTO);
		assertEquals(HttpStatus.OK.value(), genericResponseDTO.getStatusCode());

		Object resultado  = genericResponseDTO.getObjectResponse();
		assertNotNull(resultado);

		TareaDTO tareaDTO = (TareaDTO) genericResponseDTO.getObjectResponse();

		//assertNotNull(tareaDTO.getId());
		assertThat(tareaDTO, hasProperty("id", equalTo(BigInteger.ONE)));
		assertThat(tareaDTO, hasProperty("descripcion", equalTo("TAREA TEST")));
		assertThat(tareaDTO, hasProperty("vigente", equalTo("1")));
		assertNotNull(tareaDTO.getFechaCreacion());

	}

	@Test
	void actualizar() throws Exception{
		TareaDTOActualizarRequest tareaDTOActualizarRequest = TareaDTOActualizarRequest.builder().id(BigInteger.ONE).descripcion("TAREA TEST").vigente("1").build();
		TareaDAO tareaDAO = TareaDAO.builder().id(BigInteger.ONE).descripcion("TAREA TEST").vigente("1").build();
		TareaDAO tareaDAOActualizar = TareaDAO.builder().id(BigInteger.ONE).descripcion("TAREA TEST").vigente("1").fechaCreacion(new Date()).build();
		TareaDTO tareaDTOConverter = TareaDTO.builder().id(BigInteger.ONE).descripcion("TAREA TEST").vigente("1").fechaCreacion(new Date()).build();

		Optional<TareaDAO> tareaDAOOptional = Optional.of(tareaDAO);

		when(tareaRepository.findById(any(BigInteger.class))).thenReturn(tareaDAOOptional);

		when(tareaConverter.tareaDTOActualizarRequestToTareaDAO(any(TareaDTOActualizarRequest.class))).thenReturn(tareaDAO);
		when(tareaRepository.save(any(TareaDAO.class))).thenReturn(tareaDAOActualizar);
		when(tareaConverter.tareaDAOtoDTO(any(TareaDAO.class))).thenReturn(tareaDTOConverter);

		iTareaService = new TareaServiceImpl(tareaConverter , tareaRepository);

		GenericResponseDTO genericResponseDTO = iTareaService.actualizar(tareaDTOActualizarRequest);

		assertNotNull(genericResponseDTO);
		assertEquals(HttpStatus.OK.value(), genericResponseDTO.getStatusCode());

		Object resultado  = genericResponseDTO.getObjectResponse();
		assertNotNull(resultado);

		TareaDTO tareaDTO = (TareaDTO) genericResponseDTO.getObjectResponse();

		assertThat(tareaDTO, hasProperty("id", equalTo(BigInteger.ONE)));
		assertThat(tareaDTO, hasProperty("descripcion", equalTo("TAREA TEST")));
		assertThat(tareaDTO, hasProperty("vigente", equalTo("1")));
		assertNotNull(tareaDTO.getFechaCreacion());

	}

	@Test
	void eliminar() throws Exception{
		TareaDAO tareaDAO = TareaDAO.builder().id(BigInteger.ONE).descripcion("TAREA TEST").vigente("1").build();

		Optional<TareaDAO> tareaDAOOptional = Optional.of(tareaDAO);

		when(tareaRepository.findById(any(BigInteger.class))).thenReturn(tareaDAOOptional);

		iTareaService = new TareaServiceImpl(tareaConverter , tareaRepository);

		GenericResponseDTO genericResponseDTO = iTareaService.eliminar(BigInteger.ONE);

		assertNotNull(genericResponseDTO);
		assertEquals(HttpStatus.OK.value(), genericResponseDTO.getStatusCode());

		Object resultado  = genericResponseDTO.getObjectResponse();
		assertNotNull(resultado);

		BigInteger resultadoObjeto = (BigInteger) genericResponseDTO.getObjectResponse();

		assertEquals(BigInteger.ONE, resultadoObjeto);

	}

	@Test
	void listar() throws Exception{
		TareaDAO tareaDAOListar = TareaDAO.builder().id(BigInteger.ONE).descripcion("TAREA TEST").vigente("1").fechaCreacion(new Date()).build();
		TareaDTO tareaDTOListar = TareaDTO.builder().id(BigInteger.ONE).descripcion("TAREA TEST").vigente("1").fechaCreacion(new Date()).build();

		List<TareaDAO> listaTareaDAO = new ArrayList<>();
		List<TareaDTO> listaTareaDTO = new ArrayList<>();
		listaTareaDAO.add(tareaDAOListar);
		listaTareaDTO.add(tareaDTOListar);

		when(tareaRepository.findAll()).thenReturn(listaTareaDAO);
		when(tareaConverter.tareaDAOtoDTO(any(List.class))).thenReturn(listaTareaDTO);

		iTareaService = new TareaServiceImpl(tareaConverter , tareaRepository);

		GenericResponseDTO genericResponseDTO = iTareaService.listar();

		assertNotNull(genericResponseDTO);
		assertEquals(HttpStatus.OK.value(), genericResponseDTO.getStatusCode());

		Object resultado  = genericResponseDTO.getObjectResponse();
		assertNotNull(resultado);

		List<TareaDTO> resultadoObjeto = (List<TareaDTO>) genericResponseDTO.getObjectResponse();

		assertEquals(listaTareaDTO, resultadoObjeto);

	}
/*
	@Test
	void contextLoads() {
	}
*/
}
